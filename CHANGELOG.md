# 1.0.1

1. 修改post命令上传文件性能问题
2. Verified on DevEco Studio: NEXT Beta1-5.0.3.806, SDK:API12 Release(5.0.0.66)


# 1.0.0

1. DevEco Studio 版本： 4.1 Canary(4.1.3.317),OpenHarmony SDK:API11 (4.1.0.36)
2. Ark Ts 语法适配

# 0.1.0

1. 基本请求（get、post、delete...）
2. 文件上传（分片上传、断点续传）
