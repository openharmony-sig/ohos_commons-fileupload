# commons-fileupload

## 简介

commons-fileupload 是一个请求库。可以用来做文件上传（支持分片）、基本请求、文件下载。目前支持的功能，如下：
| 功能 | 描述 | 备注 |
| :--------------: | :----------------------------: | :--: |
| 基础请求 | 支持 get、post、head... |
| 文件上传 | 支持分片上传、断点续传、多文件上传 |
| 文件下载 | 未支持（后续支持） | 目前 oh 未支持 expectDataType，文件下载功能暂不支持。如果需要下载功能，建议使用 oh 原生的 request 模块

## 下载安装

```shell
ohpm install @ohos/commons-fileupload
```

OpenHarmony
ohpm 环境配置等更多内容，请参考 [如何安装OpenHarmony ohpm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md) 。

## 使用说明

### 文件上传

1.配置网络权限（在 entry\src\main\module.json5 文件中）

```
{
  "module":{
    ...,
    "requestPermissions": [
      {
        "name": "ohos.permission.INTERNET"
      },
    ]
  }
}
```

2.在页面中的使用

```
import { FormData, File, FileUpload } from "@ohos/commons_fileupload"

<!-- 在生命周期中实例化FileUpload -->
aboutToAppear() {
  this.fileUpload = new FileUpload({
    baseUrl: "https://musetransfer.com/",
    readTimeout: 60000,
    connectTimeout: 60000
  })
}

<!-- 构建formData对象 -->
const formData = new FormData();
formData.append("key", 1);//可以添加基本类型的值，目前支持string、number
formData.append("myFile", File); //也可以添加File类型的对象

<!-- 开始上传 -->
this.fileUpload.post("/api/upload", formData).then(res => {
  console.log("success");
}).catch(err => {
  console.error("fail");
})
```

## 属性（接口）说明

### FileUpload 构造函数参数

|      接口      | 参数类型 |     释义     | 是否必填 | 默认值  |
| :------------: | :------: | :----------: | :------: | ------- |
|    baseUrl     |  string  |  请求基地址  |    否    | 空      |
| connectTimeout |  number  | 连接超时时间 |    否    | 60000ms |
|  readTimeout   |  number  | 读取超时时间 |    否    | 60000ms |

### FileUpload 实例方法

|  方法   |                                类型                                 |       释义       |
| :-----: | :-----------------------------------------------------------------: | :--------------: |
|  post   | ( configOrUrl : MyHttpRequestOptions ) : Promise<http.HttpResponse> |  post 请求方法   |
|   get   | ( configOrUrl : MyHttpRequestOptions ) : Promise<http.HttpResponse> |   get 请求方法   |
|  head   | ( configOrUrl : MyHttpRequestOptions ) : Promise<http.HttpResponse> |  head 请求方法   |
| options | ( configOrUrl : MyHttpRequestOptions ) : Promise<http.HttpResponse> | options 请求方法 |
|   put   | ( configOrUrl : MyHttpRequestOptions ) : Promise<http.HttpResponse> |   put 请求方法   |
| delete  | ( configOrUrl : MyHttpRequestOptions ) : Promise<http.HttpResponse> | delete 请求方法  |
|  trace  | ( configOrUrl : MyHttpRequestOptions ) : Promise<http.HttpResponse> |  trace 请求方法  |

### MyHttpRequestOptions

|      接口      |                  参数类型                   |     释义     | 是否必填 | 默认值  |
| :------------: | :-----------------------------------------: | :----------: | :------: | ------- |
|      url       |                   string                    |   接口名字   |    是    |         |
|   extraData    | string \| Object \| ArrayBuffer \| FormData |   请求参数   |    否    | 空      |
|     header     |                   Object                    |    请求头    |    否    | 空      |
| connectTimeout |                   number                    | 连接超时时间 |    否    | 60000ms |
|  readTimeout   |                   number                    | 读取超时时间 |    否    | 60000ms |

## 约束与限制

- DevEco Studio: NEXT Beta1-5.0.3.806, SDK:API12 Release(5.0.0.66)
- DevEco Studio 版本： 4.1 Canary(4.1.3.317),OpenHarmony SDK:API11 (4.1.0.36)。

## 目录结构
````
|---- ohos_commons-fileupload
|    |---- entry  # 示例代码文件夹
|    |---- library  # commons-fileupload库文件夹
|        |---- src
|            |---- main
|                |---- ets
|                    |---- components
|                        |---- models
|                        |---- type
|                        |---- utils
|                        |---- index.ts
|                    |---- js
|        |---- Index.ets  # 对外接口
|    |---- README_zh.md  # 安装使用方法                    
````

## 贡献代码

使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/commons-fileupload/issues)
给组件，当然，也非常欢迎发 [PR](https://gitee.com/openharmony-sig/commons-fileupload/pulls) 共建。

## 开源协议

本项目基于 [Apache License 2.0](https://gitee.com/openharmony-sig/commons-fileupload/blob/master/LICENSE) ，请自由地享受和参与开源。
