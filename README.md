# commons-fileupload

## Introduction

commons-fileupload is a library used to implement file upload (including multipart upload), basic requests, and file download. Currently, the following functionalities are supported.
|   Functionality  |                Description               | Remarks|
| :------: | :--------------------------------: | :--: |
| Basic requests|      Supports basic operations, such as **get**, **post**, and **head**.      | N/A |
| File upload| Supports multipart upload, resumable upload, and multi-file upload.| N/A |
| File download| To be supported soon.| Currently, the system does not support **expectDataType**. Therefore, file download is not supported. To download files, use the native **request** module of OpenHarmony. |

## How to Install

```shell
ohpm install @ohos/commons-fileupload
```

OpenHarmony

For details about the OpenHarmony ohpm environment configuration, see [OpenHarmony HAR](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.en.md).

## How to Use

### File Upload

1. Declare the required network permission in the **entry\src\main\module.json5** file.

   ```
   {
     "module":{
       ...,
       "requestPermissions": [
         {
           "name": "ohos.permission.INTERNET"
         },
       ]
     }
   }
   ```

2. Implement file upload.

   ```
   import { FormData, File, FileUpload } from "@ohos/commons_fileupload"
   
   <!-- Instantiate FileUpload in the lifecycle -->
   aboutToAppear() {
     this.fileUpload = new FileUpload({
       baseUrl: "https://musetransfer.com/",
       readTimeout: 60000,
       connectTimeout: 60000
     })
   }
   
   <!-- Construct the formData object -->
   const formData = new FormData();
   formData.append("key", 1); // Value of basic types can be added. Currently, strings and numbers are supported.
   formData.append("myFile", File); // You can also add an object of the File type.
   
   <!-- Start uploading -->
   this.fileUpload.post("/api/upload", formData).then(res => {
     console.log("success");
   }).catch(err => {
     console.error("fail");
   })
   ```

## Available APIs

### FileUpload Constructor

|      Parameter     | Type|     Description    | Mandatory| Default Value |
| :------------: | :------: | :----------: | :------: | ------- |
|    baseUrl     |  string  |  Base address of the request. |    No   | Null     |
| connectTimeout |  number  | Connection timeout duration.|    No   | 60000 ms|
|  readTimeout   |  number  | Read timeout duration.|    No   | 60000 ms|

### FileUpload APIs

|  API  |                             Type                            |       Description      |
| :-----: | :----------------------------------------------------------: | :--------------: |
|  post   | ( configOrUrl : MyHttpRequestOptions ) : Promise<http.HttpResponse> |  **post()** method.  |
|   get   | ( configOrUrl : MyHttpRequestOptions ) : Promise<http.HttpResponse> |   **get()** method.  |
|  head   | ( configOrUrl : MyHttpRequestOptions ) : Promise<http.HttpResponse> |  **head()** method.  |
| options | ( configOrUrl : MyHttpRequestOptions ) : Promise<http.HttpResponse> | **options()** method.|
|   put   | ( configOrUrl : MyHttpRequestOptions ) : Promise<http.HttpResponse> |   **put()** method.  |
| delete  | ( configOrUrl : MyHttpRequestOptions ) : Promise<http.HttpResponse> | **delete()** method. |
|  trace  | ( configOrUrl : MyHttpRequestOptions ) : Promise<http.HttpResponse> |  **trace()** method. |

### MyHttpRequestOptions

|      Parameter     |                  Type                  |     Description    | Mandatory| Default Value |
| :------------: | :-----------------------------------------: | :----------: | :------: | ------- |
|      url       |                   string                    |   URL.  |    Yes   | Null |
|   extraData    | string \| Object \| ArrayBuffer \| FormData |   Request parameter.  |    No   | Null     |
|     header     |                   Object                    |    Request header.   |    No   | Null     |
| connectTimeout |                   number                    | Connection timeout duration.|    No   | 60000 ms|
|  readTimeout   |                   number                    | Read timeout duration.|    No   | 60000 ms|

## Constraints

- DevEco Studio: NEXT Beta1-5.0.3.806, SDK: API 12 Release (5.0.0.66)
- DevEco Studio: 4.1 Canary (4.1.3.317), OpenHarmony SDK: API 11 (4.1.0.36)

## Directory Structure
````
|---- ohos_commons-fileupload
|     |---- entry  # Sample code
|    |---- library  # commons-fileupload library code
|        |---- src
|            |---- main
|                |---- ets
|                    |---- components
|                        |---- models
|                        |---- type
|                        |---- utils
|                        |---- index.ts
|                    |---- js
|        |---- Index.ets  # APIs exposed externally
|     |---- README.md  # Readme                   
````

## How to Contribute

If you find any problem when using the project, submit an [issue](https://gitee.com/openharmony-sig/commons-fileupload/issues) or a [PR](https://gitee.com/openharmony-sig/commons-fileupload/pulls).

## License

 This project is licensed under the terms of [Apache License 2.0](https://gitee.com/openharmony-sig/commons-fileupload/blob/master/LICENSE).
